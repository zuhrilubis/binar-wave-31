import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAc87VB5pnahWaunKwWnOD4ylz5DhHCb_Q",
  authDomain: "binar-project-wave-31.firebaseapp.com",
  projectId: "binar-project-wave-31",
  storageBucket: "binar-project-wave-31.appspot.com",
  messagingSenderId: "840736104351",
  appId: "1:840736104351:web:b23019b15398d8a221a7aa",
};

// Initialize Firebase
const base = initializeApp(firebaseConfig);

export const storage = getStorage(base);
