import { useState } from "react";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage } from "./config/firebase.config";

function App() {
  const [fileMauDiUpload, setFileMauDiUpload] = useState(null);
  const [assetDiFirebaseStorage, setAssetDiFirebaseStorage] = useState(null);

  return (
    <div>
      <div style={{ margin: "50px" }}>
        <center>
          <input
            type="file"
            onChange={(e) => {
              setFileMauDiUpload(e.target.files[0]);
            }}
          />
          <button
            onClick={async () => {
              // generate date
              const date = new Date()
              const assetPath = `assets/${date.toISOString()}-${fileMauDiUpload.name}`;
              const imageRef = ref(storage, assetPath);
              const imageUploaded = await uploadBytes(
                imageRef,
                fileMauDiUpload
              );
              // simpan asset path di dalam database
              // setAssetDiFirebaseStorage(assetPath);

              getDownloadURL(imageRef).then((res) =>
                setAssetDiFirebaseStorage(res)
              );
            }}
          >
            Submit
          </button>
        </center>
      </div>
      <div>
        {assetDiFirebaseStorage && <img src={assetDiFirebaseStorage} />}
      </div>
    </div>
  );
}

export default App;
