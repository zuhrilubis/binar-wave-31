import axios from "axios";
import { useEffect, useState } from "react";

function GamePage() {
  const [loading, setLoading] = useState(true);
  const [gameList, setGameList] = useState([]);

  const getDataFromAPI = async () => {
    const accessTokenYangTersimpan = localStorage.getItem("accessToken");
    const accessToken = "Bearer " + accessTokenYangTersimpan;
    try {
      const data = await axios
        .get("https://chapter-platinum-team-2-koet.vercel.app/user/allroom", {
          headers: { Authorization: `${accessToken}` },
        })
        .then((response) => {
          return response.data.message;
        });
      setLoading(false);
      setGameList(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getDataFromAPI(); // function ini ada merubah state gameList
  }, []);

  useEffect(() => {
    console.log(gameList);
  }, [gameList]);

  return (
    <div style={{ display: "flex", flexWrap: "wrap" }}>
      {loading ? <div>Loading... </div> : <></>}
      {gameList?.map((x, i) => (
        <div
          style={{
            margin: "10px",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
            width: "200px",
            padding: "10px",
            border: "1px solid black",
          }}
          key={i}
        >
          <div>{x.roomName}</div>
          <div>{x?.winner?.player2Name}</div>
        </div>
      ))}
    </div>
  );
}

export default GamePage;
