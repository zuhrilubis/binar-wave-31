import React, { useState } from "react";

export default function SinglePost() {
  const [post, setPost] = useState({
    userId: 1,
    id: 1,
    writer: "J.K.R",
    firstName: "Kelly",
    lastName: "Anderson",
    title: "Harry Potter",
    body: "Expecto Patroneum",
  });

  return (
    <div>
      <table style={{ border: "1px solid black" }}>
        <tr>
          <td>Penulis</td>
          <td>:</td>
          {/* <td>{    `${post.firstName} ${post.lastName}`   }</td> */}
          <td>{post.firstName} {post.lastName}</td>
        </tr>
        <tr>
          <td>Judul</td>
          <td>:</td>
          <td>{post.title}</td>
        </tr>
        <tr>
          <td>Body</td>
          <td>:</td>
          <td>{post.body}</td>
        </tr>
      </table>
    </div>
  );
}
