import React, { useState } from "react";

export default function Blog() {
  const [judul, setJudul] = useState("Judul");

  const [dataUser, setDataUser] = useState({
    username: "elsetiyawan",
    role: "editor",
  });

  return (
    <div style={{ margin: "30px" }}>
      <label>Judul</label>
      <input
        value={judul}
        onChange={(e) => {
        //   if (dataUser.role !== "viewer") {
            setJudul(e.target.value);
        //   }
        }}
        disabled={dataUser.role === "viewer" ? true : false}
      />
    </div>
  );
}
