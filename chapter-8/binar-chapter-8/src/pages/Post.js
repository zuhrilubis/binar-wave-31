import React from "react";
import { useParams } from "react-router-dom";

export default function Post() {
  const { id, userId } = useParams();
  return <div>Post Single id : {id} - userid: {userId}</div>;
}
