import React from "react";
import { Link } from "react-router-dom";

export default function Dashboard() {
  return (
    <div>
      <div>Dashboard page</div>
      <div>
        <Link to={"/login"}>
          <div>Klik aku untuk ke halaman login</div>
        </Link>
      </div>
      <div>
        <Link to={"/registration"}>
          <div>Klik aku untuk ke halaman registration</div>
        </Link>
      </div>
    
    </div>
  );
}
