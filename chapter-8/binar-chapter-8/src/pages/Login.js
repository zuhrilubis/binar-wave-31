import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
export default function Login() {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const navigate = useNavigate();

  const handleLogin = async () => {
    console.log(username);
    console.log(password);
    try {
      const hasilHitAPI = await axios.post(
        "https://binar-app-elsetiyawan.vercel.app/login",
        { username: username, password: password }
      );
      const token = hasilHitAPI.data;
      console.log(token);
    } catch (error) {
      alert("Ada error!");
    }
  };

  return (
    <div style={{ marginTop: "50px" }}>
      <center>
        <input
          placeholder="Username"
          onChange={(e) => {
            setUsername(e.target.value);
          }}
        />
        <input
          placeholder="Password"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />

        <button onClick={handleLogin}>Submit</button>
      </center>
    </div>
  );
}
