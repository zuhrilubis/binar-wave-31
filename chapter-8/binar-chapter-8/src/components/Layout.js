import React from "react";
import { Link, Outlet } from "react-router-dom";

export default function Layout() {
  return (
    <div>
      <div
        style={{
          display: "flex",
          paddingLeft: "50px",
        }}
      >
        <Link to="/dashboard">
          <div
            style={{
              marginLeft: "10px",
              cursor: "pointer",
              backgroundColor: "lightgreen",
            }}
          >
            Dashboard
          </div>
        </Link>
        <Link to="/registration">
          <div
            style={{
              marginLeft: "10px",
              cursor: "pointer",
              backgroundColor: "lightcoral",
            }}
          >
            Register
          </div>
        </Link>
        <Link to="/login">
          <div
            style={{
              marginLeft: "10px",
              cursor: "pointer",
              backgroundColor: "lightsalmon",
            }}
          >
            Login
          </div>
        </Link>
      </div>
      <div>
        <Outlet />
      </div>
    </div>
  );
}
