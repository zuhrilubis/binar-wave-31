import Button from "./Button";

const Kiri = () => {
  return (
    <div
      style={{
        backgroundColor: "orange",
        width: "50vw",
        padding: "20px",
      }}
    >
      Kiri
      <div>
        <Button title="Klik Kiri" />
      </div>
    </div>
  );
};

export default Kiri;
