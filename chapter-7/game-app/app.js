const express = require("express");
const app = express();
const port = 8000;
const userRouter = require("./users/users.route");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const gameSwagger = require("./gameSwagger.json");
require('dotenv').config();

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(gameSwagger));

app.get("/", (req, res) => {
  return res.sendFile("index.html");
});

app.use("/users", userRouter);

app.get("/ping", (req, res) => {
  return res.json({ message: "pong" });
});

app.listen(port, () => {
  console.log(`App is running on port ${port}`);
});
