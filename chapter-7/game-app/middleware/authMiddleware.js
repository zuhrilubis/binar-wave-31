const jwtTool = require("jsonwebtoken");
require('dotenv').config();

const authMiddleware = async (req, res, next) => {
  //   cek apakah ada Authorization didalam header
  //   ambil key authorization dalam header
  const { authorization } = req.headers;

  // jika authorization undefined / null maka reject request
  if (authorization === undefined) {
    res.statusCode = 400;
    return res.json({ message: "Unauthorized" });
  }

  try {
    const splitedToken = authorization.split(" ")[1]

    // cek apakah autorization valid, kalau tidak valid return Unauthorized, jika valid lanjutkan proses
    const token = await jwtTool.verify(
      splitedToken,
      process.env.SECRET_KEY
    );

    next();
  } catch (error) {
    res.statusCode = 400;
    return res.json({ message: "Invalid token" });
  }
};

module.exports = authMiddleware;
