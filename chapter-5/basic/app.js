// CORE MODULE
// Module bawaan saat install nodejs

// const fs = require("fs");

// try {
//   const data = fs.readFileSync("./README.txt", "utf8");
//   console.log(data);
// } catch (err) {
//   console.error(err);
// }

// ================================================================

// LOCAL MODULE
// const { tambah, variableX } = require("./calculator");

// const hasilTambah = tambah(10, 12);
// console.log(variableX);

// ================================================================
// THIRT PARTY MODULE / Dependency / Library

// const {sqrt} = require("mathjs"); // common js
// // import {sqrt} from "mathjs"; // es6

// const square = sqrt(4)
// console.log(square);

const http = require("http");

const hostname = "127.0.0.1";
const port = 8000;

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    res.statusCode = 200;
    res.end("Methode GET");
  } else if (req.method === "POST") {
    res.statusCode = 200;
    res.end("Methode POST");
  } else {
    res.statusCode = 200;
    res.end("Methode selain GET dan POST");
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
