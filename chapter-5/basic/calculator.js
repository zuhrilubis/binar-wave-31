const tambah = (x, y) => {
  return x + y;
};

const kurang = (x, y) => {
  return x - y;
};

const variableX = 10;

module.exports = {
  tambah,
  kurang,
  variableX,
};
