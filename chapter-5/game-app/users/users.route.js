const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");

userRouter.get("/", userController.getAllUsers);

userRouter.post("/registration", userController.registerUsers);

userRouter.post("/login", userController.userLogin);

module.exports = userRouter;
