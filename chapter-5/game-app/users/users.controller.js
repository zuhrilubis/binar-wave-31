const userModel = require("./users.model");

class UserController {
  getAllUsers = (req, res) => {
    const allUsers = userModel.getAllUser();
    return res.json(allUsers);
  };

  registerUsers = (req, res) => {
    const dataRequest = req.body;
    //  cek apakah username, email dan password exists
    if (dataRequest.username === undefined || dataRequest.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username is invalid" });
    }

    if (dataRequest.email === undefined || dataRequest.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email is invalid" });
    }

    if (dataRequest.password === undefined || dataRequest.password === "") {
      res.statusCode = 400;
      return res.json({ message: "Password is invalid" });
    }

    //   cek apakah username dan email sudah teregistrasi
    const existData = userModel.isUserRegistered(dataRequest);

    if (existData) {
      return res.json({ message: "Username or email is exist!" });
    }

    //   record data kedalam userList
    userModel.recordNewData(dataRequest);

    return res.json({ message: "New user is recorded" });
  };

  userLogin = (req, res) => {
    const { username, password } = req.body;
    const dataLogin = userModel.verifyLogin(username, password);

    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      return res.json({ message: "Invalid credential" });
    }
  };
}

module.exports = new UserController();
