const express = require("express");
const app = express();
const port = 8000;

const movieList = [
  {
    title: "The Lost World",
    year: "2015",
  },
  {
    title: "The Matrix",
    year: "1999",
  },
  {
    title: "Harry Potter",
    year: "2008",
  },
];

app.use(express.json());

app.get("/", (req, res) => {
  return res.json({ message: "Hi there" });
});

app.post("/", (req, res) => {
  const data = req.body;
  return res.json({ message: data });
});

app.get("/ping", (req, res) => {
  return res.json({ message: "pong" });
});

// API to get all the movie
app.get("/movies", (req, res) => {
  // cara 1
  const queryData = req.query;
  const sortedValue = movieList.find((movie) => {
    return movie.title === queryData.title;
  });
  if(sortedValue === undefined){
    res.statusCode = 404;
    return res.json({message: "Movie is not exist!"})
  }

  return res.json(sortedValue);
});

// cara 2
app.post("/movies", (req, res) => {
  const dataBody = req.body;
  const sortedValue = movieList.find((movie) => {
    return movie.title === dataBody.title;
  });
  if (sortedValue === undefined) {
    return res.json({ message: "Movie is not exists!" });
  }

  return res.json(sortedValue);
});

app.listen(port, () => {
  console.log("App is running on port " + port);
});
