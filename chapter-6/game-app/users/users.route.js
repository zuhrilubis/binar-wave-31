const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");

userRouter.get("/", userController.getAllUsers);

userRouter.post("/registration", userController.registerUsers);

userRouter.post("/login", userController.userLogin);

userRouter.get("/detail/:idUser", userController.getSingleUser);

userRouter.put("/detail/:idUser", userController.updateUserBio);

userRouter.get("/games", userController.getAllGames);

module.exports = userRouter;
