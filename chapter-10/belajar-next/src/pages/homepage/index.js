import Link from "next/link";
import React, { useState } from "react";
import styles from '../../styles/Homepage.module.css'

function Homepage() {
  const [count, setCount] = useState(0);

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div>
        <div>Ini adalah homepage</div>
        <div>{count}</div>
        <div>
          <button
          className="btn btn-warning"
            onClick={() => {
              setCount(count + 1);
            }}
          >
            Tambah
          </button>
        </div>
        <div>
          <Link href={"/games"}>Link to Games</Link>
        </div>
      </div>
    </div>
  );
}

export default Homepage;
