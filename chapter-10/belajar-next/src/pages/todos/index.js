import axios from "axios";
import React from "react";

export default function Todos(props) {
  return (
    <div style={{ margin: 100, display: "flex", flexWrap: "wrap" }}>
      {props.todoList.map((todo) => (
        <div
          style={{ width: 100, border: "1px solid white", margin: 10 }}
          key={todo.id}
        >
          {todo.title}
        </div>
      ))}
    </div>
  );
}

export const getServerSideProps = async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/todos"
  );
  return {
    props: {
      todoList: response.data,
    },
  };
};
