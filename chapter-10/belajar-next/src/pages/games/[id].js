import { useRouter } from "next/router";
import React from "react";

function GameWithId() {
  const routing = useRouter();

  return <div>GameWithId : {routing.query.id} </div>;
}

export default GameWithId;
