import axios from "axios";
import React from "react";

export default function Users(props) {
  return (
    <div style={{ margin: 100 }}>
      {props.listUsers.map((user) => (
        <div key={user.id}>{user.name}</div>
      ))}
    </div>
  );
}

export const getStaticProps = async () => {
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/users");
    return {
      props: { listUsers: res.data },
    };
  } catch (error) {
    return {
      props: { listUsers: [] },
    };
  }
};
