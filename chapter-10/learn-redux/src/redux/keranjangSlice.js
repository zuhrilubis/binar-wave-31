import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  item: "",
//   jumlah item
};

export const keranjangSlice = createSlice({
  name: "keranjang",
  initialState,
  reducers: {
    setItem: (state, action) => {
      state.item = action.payload;
    },
    resetItem: (state) => {
      state.item = "";
    },
    // action untuk update jumlah item
  },
});

export const { setItem, resetItem } = keranjangSlice.actions;

export default keranjangSlice.reducer;
