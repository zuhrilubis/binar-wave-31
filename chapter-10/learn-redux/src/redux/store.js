import { configureStore } from "@reduxjs/toolkit";
import keranjangSlice from "./keranjangSlice";
import catSlice from "./catSlice";

export const store = configureStore({
  reducer: {
    keranjang: keranjangSlice,
    cats: catSlice,
  },
});
