import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCatFacts, setFacts } from "../redux/catSlice";

function CatFatcsThunk() {
  const catFactsRedux = useSelector((state) => state.cats);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCatFacts());
  }, []);

  const handleClick = () => {
    /**
     * OPSI 1
     * 1. kiri ke backend post(....)
     * 2. dispatch(fetchCatFacts())
     */
    /**
     * OPSI 2
     * 1. kiri ke backend post(....)
     * 2. update reduxnya langsung
     *    dispatch(tambahkanFacts(pakedatayangdikirim))
     */
    // ketika create fact, dibackend akan create id
    // id nya gimana???
  };

  return (
    <div>
      <div>CatFatcsThunk</div>
      <input></input>
      <button onClick={handleClick}>SUbmit</button>
    </div>
  );
}

export default CatFatcsThunk;
