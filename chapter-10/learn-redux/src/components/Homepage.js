import React, { useState } from "react";

function Homepage() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <div>Ini adalah homepage</div>
      <div>{count}</div>
      <div>
        <button
          onClick={() => {
            setCount(count + 1);
          }}
        >
          Tambah
        </button>
      </div>
    </div>
  );
}

export default Homepage;
