import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function DataDIri() {
  const globalState = useSelector((state) => state.user);
  console.log(globalState);
  const [username, setUsername] = useState("ini ada di data diri");
  return (
    <div>
      <div>DataDiri : {username}</div>
      <div>
        <Link to="/">Back to Form</Link>
      </div>
    </div>
  );
}

export default DataDIri;
