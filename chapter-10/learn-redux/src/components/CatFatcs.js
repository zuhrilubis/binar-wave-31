import axios from 'axios';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setFacts } from '../redux/catSlice';

// manual set redux
// call api 
// then set redux
function CatFatcs() {
  const catFactsRedux = useSelector(state => state.cats);
  const dispatch = useDispatch();
  const dapatkaDataCatFacts = async () => {
    try {
      const catFactsDariAPI = await axios.get('https://cat-fact.herokuapp.com/facts');
      dispatch(setFacts(catFactsDariAPI.data));
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(()=> {
    dapatkaDataCatFacts();
  }, []);
  
  return (
    <div>CatFatcs</div>
  )
}

export default CatFatcs