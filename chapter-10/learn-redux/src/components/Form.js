import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function Form() {
  const globalState = useSelector((state) => state.user);
  const [username, setUsername] = useState(".....");
  console.log(globalState);
  return (
    <div>
      <div>
        <label>Username: </label>
        <input onChange={(e) => setUsername(e.target.value)} />
      </div>
      <div>hello '{username}'</div>
      <div>
        <Link to="data-diri">Data Diri Page</Link>
      </div>
    </div>
  );
}

export default Form;
