const express = require('express');
const usersController = require('./users.controller');
const userRouter = express.Router();

userRouter.get('/', usersController.getAllUsers)


module.exports = userRouter;