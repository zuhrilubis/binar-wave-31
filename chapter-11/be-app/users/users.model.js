const { userList } = require("./users.constants");

class UserModel {
    getAllUsers = () => {
        return userList;
    }
}

module.exports = new UserModel();