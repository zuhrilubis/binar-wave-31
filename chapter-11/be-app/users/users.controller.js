const usersModel = require("./users.model");

class UserController {
    getAllUsers = (req, res) => {
        const userList = usersModel.getAllUsers();
        return res.status(200).json(userList)
    }
}

module.exports = new UserController();