import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Video from "./pages/Video";
import Pdf from "./pages/Pdf";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/video" element={<Video />} />
        <Route path="/pdf" element={<Pdf />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
