// const rockBox = document.getElementById("rock");
// const paperBox = document.getElementById("paper");
// const scissorBox = document.getElementById("scissor");

let pilihanUser = "";
let pilihanComputer = "";

class Computer {
  constructor(name){
    this.name = name
  }
  
  computerMemilih = () => {
    const pilihanYangTersedia = ["comRock", "comPaper", "comScissor"];
    const rndInt = Math.floor(Math.random() * 3);
    const pilihanKomputer = pilihanYangTersedia[rndInt];
    pilihanComputer = pilihanYangTersedia[rndInt];
    this.kasihBackgroundMerah(pilihanKomputer);
    hasilPertandingan();
  };

  kasihBackgroundMerah = (pilihan) => {
    const selectedBox = document.getElementById(pilihan);
    selectedBox.style.backgroundColor = "red";
  };
}

const komputer = new Computer();

class Options {
  constructor(pilihan) {
    this.pilihan = pilihan;
    this.option = document.getElementById(pilihan);
  }

  onclick = () => {
    this.option.style.backgroundColor = "grey";
    pilihanUser = this.pilihan;
    disableClick()
    komputer.computerMemilih();
  };
}

const rockOption = new Options('rock');
const paperOption = new Options('paper');
const scissorOption = new Options('scissor');

rockOption.option.onclick = () => rockOption.onclick();
paperOption.option.onclick = () => paperOption.onclick();
scissorOption.option.onclick = () => scissorOption.onclick();

// rockBox.onclick = () => {
//   rockBox.style.backgroundColor = "grey";
//   pilihanUser = "rock";
//   komputer.computerMemilih()
//   disableClick();
// };

// paperBox.onclick = () => {
//   paperBox.style.backgroundColor = "grey";
//   pilihanUser = "paper";
//   komputer.computerMemilih()
//   disableClick();
// };

// scissorBox.onclick = () => {
//   scissorBox.style.backgroundColor = "grey";
//   pilihanUser = "scissor";
//   komputer.computerMemilih()
//   disableClick();
// };

const disableClick = () => {
  rockOption.option.style.pointerEvents = "none";
  paperOption.option.style.pointerEvents = "none";
  scissorOption.option.style.pointerEvents = "none";
};

const hasilPertandingan = () => {
  if (pilihanUser === "rock" && pilihanComputer === "comRock") {
    console.log("DRAW");
  } else if (pilihanUser === "paper" && pilihanComputer === "comPaper") {
    console.log("DRAW");
  } else {
    console.log("SOMEONE WIN");
  }
};
